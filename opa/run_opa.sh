#!/usr/bin/env bash
set -eu

WORKDIR=$(dirname $0)
OPA_PORT=8181
OPA_SERVER=http://localhost:${OPA_PORT}
POLICY=${OPA_SERVER}/v1/policies/rbac ## rbac es el identificador de la policies
DATA=${OPA_SERVER}/v1/data/cl/banchile/rbac ## ruta de la data que se utilizara en proceso de validacion de la policies

if [[ $(curl -s ${POLICY} | jq .result.id) != "rbac" ]]; then
    echo "Uploading Policy"
    curl -T "${WORKDIR}/authz.rego" -X PUT ${POLICY}
fi

if [[ $(curl -s ${DATA} | jq .result.id) != "rbac" ]]; then
    echo "Uploading data"
    curl -T "${WORKDIR}/data.json" -X PUT ${DATA}
fi
