# JWT Authorization Policy
# This should be loaded to the OPA Policy Server via a PUT request to the /v1/policies endpoint.

package cl.banchile.rbac
import future.keywords.in
import future.keywords.if

import data.cl.banchile.rbac.idp
import data.cl.banchile.rbac.permissions

token_header := t[0] {
    t := io.jwt.decode(input.api_token)
}

token_body := t[1] {
    t := io.jwt.decode(input.api_token)
}

is_token_header_valid{
 token_header.alg == idp.alg
 token_header.typ == idp.jwt
}

is_token_valid{
 token_body.iss == idp.iss
 token_body.tid == idp.tenant
}

user := u {
    token_body.iss == idp.iss
    u = token_body.name
}

roles := r {
    r = token_body.roles
}

is_user {
    count(roles) > 0
}

default allow := false

allow if {
	r := roles[_]
    vpermissions := permissions[r]
    p := vpermissions[_]
    p == {"action": input.resource.method, "object": input.resource.service}
}
